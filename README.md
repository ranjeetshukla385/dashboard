# Dashboard project on Next JS

## Feature

## How to use

Download the example [or clone the repo](https://gitlab.com/ranjeetshukla385/dashboard.git):

```sh
cd dashboard
```

Install it and run:

```sh
npm install
npm run dev
```

## Other Info

```sh
Fake API endpoints : https://reqres.in/
Random user generator : https://randomuser.me/api/
Guid generator : https://www.guidgenerator.com/online-guid-generator.aspx
```
