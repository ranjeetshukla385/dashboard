-- Up
CREATE TABLE users (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  email TEXT NOT NULL,
  first_name TEXT  NOT NULL,
  last_name TEXT  NOT NULL,
  avatar TEXT NOT NULL,
  password TEXT
);

--INSERT INTO users (email,first_name,last_name,avatar,password) VALUES ('michael.lawson@reqres.in','Michael','Lawson', 'https://s3.amazonaws.com/uifaces/faces/twitter/follettkyle/128.jpg','QpwL5tke4Pnpja7X4');
--INSERT INTO users (email,first_name,last_name,avatar,password) VALUES ('lindsay.ferguson@reqres.in','Lindsay','Ferguson', 'https://s3.amazonaws.com/uifaces/faces/twitter/araa3185/128.jpg','QpwL5tke4Pnpja7X4');
--INSERT INTO users (email,first_name,last_name,avatar,password) VALUES ('tobias.funke@reqres.in','Tobias','Funke', 'https://s3.amazonaws.com/uifaces/faces/twitter/vivekprvr/128.jpg','QpwL5tke4Pnpja7X4');

-- Down
DROP TABLE users;