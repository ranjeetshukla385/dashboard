import React from "react";
import FullPage from "../Components/FullPage";
import MTable from "../Components/MTable";
import { fetchData } from "../utils/myFetch";
import { Router } from "next/router";

export default function Users({ props }) {
  console.log(props);

  function createData(avatar, first_name, last_name, email) {
    return { avatar, first_name, last_name, email };
  }
  const columns = [
    { id: "avatar", label: "Avatar", minWidth: 50, type: "image" },
    { id: "first_name", label: "First Name", minWidth: 100 },
    { id: "last_name", label: "Last Name", minWidth: 100 },
    { id: "email", label: "Email", minWidth: 100 },
  ];
  // const rows = [
  //   createData(
  //     "https://randomuser.me/api/portraits/women/92.jpg",
  //     "Ranjeet",
  //     "shukla",
  //     "ranjeetshukla385@gmail.com"
  //   ),
  // ];

  const rows = props.map((user) =>
    createData(user.avatar, user.first_name, user.last_name, user.email)
  );

  return (
    <FullPage>
      <MTable columns={columns} rows={rows} />
    </FullPage>
  );
}

Users.getInitialProps = async (ctx) => {
  const cookie = ctx?.req?.headers.cookie;
  const res = await fetch("http://localhost:3000/api/users", {
    headers: {
      cookie: cookie,
    },
  });
  if (res.status === 401 && !ctx.req) {
    Router.replace("/login");
    return {};
  }
  if (res.status === 401 && ctx.req) {
    ctx.res?.writeHead(302, { location: "http://localhost:3000/login" });
    ctx.res?.end();
    return;
  }
  const data = await res.json();
  return { props: data };
};
