import React from "react";
import FullPage from "../Components/FullPage";
import Link from "next/link";

export default function Dashboard() {
  return (
    <FullPage>
      <Link href="/users">Users</Link>
    </FullPage>
  );
}
