import sqlite from "sqlite";

export default async function getAllUsers(req, res) {
  const db = await sqlite.open("./mydb.sqlite");
  if (req.method === "PUT") {
    const statement = await db.prepare(
      "UPDATE users SET email = ?, first_name = ?, last_name = ? Where id = ?"
    );
    const result = await statement.run(
      req.body.email,
      req.body.first_name,
      req.body.last_name,
      req.query.id
    );
    result.finalize();
  }

  const users = await db.get(
    "SELECT id, email, first_name, last_name, avatar from users WHERE id = ?",
    [req.query.id]
  );
  res.json(users);
}
