import sqlite from "sqlite";
import { authenticator } from "../../Components/authenticator";

export default authenticator(async function getAllUsers(req, res) {
  console.log(process.env.NODE_ENV);
  if (req.method !== "GET") {
    res.status(500).json({ message: "Sorry we support only GET requests" });
  }
  const db = await sqlite.open("./mydb.sqlite");
  const users = await db.all(
    "SELECT id,email, first_name,last_name,avatar from users"
  );
  res.json(users);
});
