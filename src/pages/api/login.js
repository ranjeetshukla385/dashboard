import sqlite from "sqlite";
import { compare } from "bcrypt";
import { sign } from "jsonwebtoken";
import { secret } from "../../Components/secret";
import cookie from "cookie";

export default async function login(req, res) {
  if (req.method === "POST") {
    const db = await sqlite.open("./mydb.sqlite");
    const users = await db.get("SELECT * from users WHERE email = ?", [
      req.body.email,
    ]);
    compare(req.body.password, users?.password, function (err, result) {
      if (!err && result) {
        const payload = { id: users.id, email: users.email };
        const jwtToken = sign(payload, secret, { expiresIn: "1h" });

        res.setHeader(
          "Set-Cookie",
          cookie.serialize("authToken", jwtToken, {
            httpOnly: true,
            secure: process.env.NODE_ENV !== "development",
            sameSite: "strict",
            maxAge: 60 * 60, //1 hr because out token will expire in 1 hour
            path: "/",
          })
        );

        res.status(200).json({ message: "Logged in..." });
      } else {
        res.status(401).json({ message: "Unauthorized" });
      }
    });
  } else {
    res
      .status(405)
      .json({ message: "Invalid Method, We only support POST for this API" });
  }
}
