import sqlite from "sqlite";
import { hash } from "bcrypt";

export default async function signup(req, res) {
  if (req.method === "POST") {
    hash(req.body.password, 10, async function (err, hash) {
      if (err) {
        res.status(503).json({
          message: "Some problem with bcrypt hash not generated!",
        });
      }

      const db = await sqlite.open("./mydb.sqlite");
      const statement = await db.prepare(
        "INSERT INTO users(email, first_name, last_name, avatar, password) values ( ?, ?, ?, ?, ?)"
      );
      const result = await statement.run(
        req.body.email,
        req.body.first_name,
        req.body.last_name,
        req.body.avatar,
        hash
      );
      result.finalize();

      // Get all user after insert
      // const users = await db.all(
      //   "SELECT id, email, first_name, last_name, avatar, password from users"
      // );
      res.status(200).json({ message: "You are registered successfully" });
    });
  } else {
    res
      .status(405)
      .json({ message: "Invalid Method, We only support POST for this API" });
  }
}
