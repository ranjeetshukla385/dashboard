import React from "react";
import Alert from "@material-ui/lab/Alert";

export function MAlert_success(props) {
  return <Alert severity="success">{props.children}</Alert>;
}

export function MAlert_error(props) {
  return <Alert severity="error">{props.children}</Alert>;
}
