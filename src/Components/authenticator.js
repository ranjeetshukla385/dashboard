import { secret } from "./secret";
import { verify } from "jsonwebtoken";

export const authenticator = (fn) => (req, res) => {
  verify(req.cookies.authToken, secret, async function (err, decoded) {
    if (!err && decoded) {
      return await fn(req, res);
    }
    res.status(401).json({ message: "Sorry, you are not authenticated" });
  });
};
