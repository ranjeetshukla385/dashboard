import React from "react";
import Header from "./Header";
import Footer from "./Footer";

export default function FullPage(props) {
  return (
    <>
      <Header title={props?.headerTitle} />
      {props.children}
      <Footer />
    </>
  );
}
