export async function getLogin(url = "", data = {}, method = "POST") {
  // Default options are marked with *
  const response = await fetch(url, {
    method: method, // *GET, POST, PUT, DELETE, etc.
    mode: "cors", // no-cors, *cors, same-origin
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    credentials: "same-origin", // include, *same-origin, omit
    headers: {
      "Content-Type": "application/json",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: "follow", // manual, *follow, error
    referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data), // body data type must match "Content-Type" header
  });
  //console.log(response.status);
  return response; // parses JSON response into native JavaScript objects
}

export async function fetchData(
  url = "",
  data = {},
  method = "POST",
  cookie = ""
) {
  let otherParams = {};
  if (method === "GET") {
    otherParams = {
      method: method, // *GET, POST, PUT, DELETE, etc.
      headers: {
        "Content-Type": "application/json",
        cookie: cookie,
      },
    };
  }
  if (method === "POST") {
    otherParams = {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
  }

  return fetch(url, otherParams)
    .then((res) => {
      if (res.ok) {
        return res;
      } else {
        return {
          status: "error",
          statusCode: res.status,
          statusText: res.statusText,
        };
      }
    })
    .then((finalRespose) => finalRespose)
    .catch((err) => console.log("Error in fetch API !"));
}
