const sqlite = require("sqlite");

async function setup() {
  const db = await sqlite.open("./mydb.sqlite");
  await db.migrate({ force: "last" });

  const users = await db.all("SELECT * from users");
  console.log("ALL USERS :", users);
  console.info("Database has been created.");
}

setup();

//https://www.sqlitetutorial.net/tryit/query/sqlite-autoincrement/#7
